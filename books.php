<?php

/**
 * Plugin Name: books
 * Plugin URI: http://localhost/wordpress/wp-content/plugins/books
 * Description: détermine les détails de chaque livres
 * Version: 20200414
 * Author: Olivier Munda
 * Author URI: https://omunda@labo-ve.fr/
 * License: GPLv2
 */

// Define path and URL to the current plugin.
define('OM_BOOKS_PATH', plugin_dir_path(__FILE__));

// Define path and URL to the ACF plugin.
define('OM_ACF_PATH', OM_BOOKS_PATH . '/includes/acf/');
define('OM_ACF_URL', plugin_dir_url(__FILE__) . '/includes/acf/');

// Include the ACF plugin.
include_once(OM_ACF_PATH . 'acf.php');

// Customize the url setting to fix incorrect asset URLs.

function om_acf_settings_url($url)
{
	return OM_ACF_URL;
}
add_filter('acf/settings/url', 'om_acf_settings_url');

// (Optional) Hide the ACF admin menu item.
// function om_acf_settings_show_admin($show_admin)
// {
// 	return false;
// }
// add_filter('acf/settings/show_admin', 'om_acf_settings_show_admin');

// Custom Post Type om_books definition
if (!function_exists('om_books_cpt')) {

	// Register Custom Post Type
	function om_books_cpt()
	{
		$labels = array(
			'name'                  => _x('books', 'Post Type General Name', 'om_books_text'),
			'singular_name'         => _x('book', 'Post Type Singular Name', 'om_books_text'),
			'menu_name'             => __('Books', 'om_books_text'),
			'name_admin_bar'        => __('Books', 'om_books_text'),
			'archives'              => __('Item Archives', 'om_books_text'),
			'attributes'            => __('Item Attributes', 'om_books_text'),
			'parent_item_colon'     => __('Parent Book:', 'om_books_text'),
			'all_items'             => __('All Books', 'om_books_text'),
			'add_new_item'          => __('Add New Book', 'om_books_text'),
			'add_new'               => __('Add Book', 'om_books_text'),
			'new_item'              => __('New Book', 'om_books_text'),
			'edit_item'             => __('Edit Book', 'om_books_text'),
			'update_item'           => __('Update Book', 'om_books_text'),
			'view_item'             => __('View Book', 'om_books_text'),
			'view_items'            => __('View Books', 'om_books_text'),
			'search_items'          => __('Search Book', 'om_books_text'),
			'not_found'             => __('Not found', 'om_books_text'),
			'not_found_in_trash'    => __('Not found in Trash', 'om_books_text'),
			'featured_image'        => __('Featured Image', 'om_books_text'),
			'set_featured_image'    => __('Set featured image', 'om_books_text'),
			'remove_featured_image' => __('Remove featured image', 'om_books_text'),
			'use_featured_image'    => __('Use as featured image', 'om_books_text'),
			'insert_into_item'      => __('Insert into item', 'om_books_text'),
			'uploaded_to_this_item' => __('Uploaded to this item', 'om_books_text'),
			'items_list'            => __('Items list', 'om_books_text'),
			'items_list_navigation' => __('Items list navigation', 'om_books_text'),
			'filter_items_list'     => __('Filter items list', 'om_books_text'),
		);
		$rewrite = array(
			'slug'                  => 'Books',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __('book', 'om_books_text'),
			'description'           => __('donne les renseignements utiles sur les éditions de livres', 'om_books_text'),
			'labels'                => $labels,
			'supports'              => array('title', 'editor', 'thumbnail'),
			// 'taxonomies'            => array('Genre', 'Image', 'Auteurs', 'Editeurs', 'Date d\'édition', 'Nombre de pages', ' format', 'post_tag'),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 10,
			'menu_icon'             => 'dashicons-book-alt',
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'post',
			'show_in_rest'          => true,
		);
		register_post_type('om_books', $args);
	}
	add_action('init', 'om_books_cpt', 0);
}
// déclaration taxonomie
// register_taxonomy('genres', 'books', $args);

// add_action('init', 'om_books_cpt');

// generate php code ACF

if (function_exists('acf_add_local_field_group')) :

	acf_add_local_field_group(array(
		'key' => 'group_5e95d839360db',
		'title' => 'Book informations',
		'fields' => array(
			array(
				'key' => 'field_5e9d926da6acc',
				'label' => 'Titre',
				'name' => 'books_titre',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5e9d92dff4a73',
				'label' => 'Description',
				'name' => 'books_description',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'wpautop',
			),
			array(
				'key' => 'field_5e9d93377c2b6',
				'label' => 'Résumé',
				'name' => 'books_resume',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'wpautop',
			),
			array(
				'key' => 'field_5e95d84ef64e8',
				'label' => 'Genre',
				'name' => 'books_genre',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'category',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
			array(
				'key' => 'field_5e95dc4ddee2b',
				'label' => 'Auteurs',
				'name' => 'books_auteurs',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5e95d84ef64e8',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'post_tag',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 1,
				'load_terms' => 1,
				'return_format' => 'object',
				'multiple' => 0,
			),
			array(
				'key' => 'field_5e95dc98dee2d',
				'label' => 'Editeur',
				'name' => 'books_editeur',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5e95d84ef64e8',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'post_tag',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 1,
				'load_terms' => 1,
				'return_format' => 'object',
				'multiple' => 0,
			),
			array(
				'key' => 'field_5e95dcb9dee2e',
				'label' => 'Date d\'édition',
				'name' => 'books_date_dedition',
				'type' => 'date_picker',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5e95d84ef64e8',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'display_format' => 'Y',
				'return_format' => 'Y',
				'first_day' => 1,
			),
			array(
				'key' => 'field_5e95dcd2dee2f',
				'label' => 'Nombre de pages',
				'name' => 'books_nombre_de_pages',
				'type' => 'number',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5e95d84ef64e8',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array(
				'key' => 'field_5e95dce7dee30',
				'label' => 'Format',
				'name' => 'books_format',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'post_tag',
				'field_type' => 'multi_select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 1,
				'load_terms' => 1,
				'return_format' => 'object',
				'multiple' => 0,
			),
			array(
				'key' => 'field_5e96bfa5f7b9c',
				'label' => 'Image',
				'name' => 'books_image',
				'type' => 'image',
				'instructions' => 'Mini : 200 x 200 px
	Maxi : 1000 x 1000 px
	Only	GIF, JPG and JPEG',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'full',
				'library' => 'all',
				'min_width' => 200,
				'min_height' => 200,
				'min_size' => '',
				'max_width' => 1000,
				'max_height' => 1000,
				'max_size' => 4,
				'mime_types' => 'jpg, jpeg, gif',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'left',
		'instruction_placement' => 'label',
		'hide_on_screen' => array(
			0 => 'the_content',
			1 => 'excerpt',
			2 => 'discussion',
			3 => 'comments',
			4 => 'slug',
			5 => 'author',
			6 => 'format',
			7 => 'featured_image',
			8 => 'categories',
			9 => 'tags',
		),
		'active' => true,
		'description' => '',
	));

endif;

// Register new-book ACF front form
function om_acf_form_init()
{

	// Check function exists.
	if (function_exists('acf_register_form')) {

		// Register form.
		acf_register_form(array(
			'id'                    => 'new-book',
			'post_id'               => 'new_post',
			'new_post'              => array(
				'post_type'   => 'om_books',
				'post_status' => 'publish',
			),
			'field_groups'          => array(
				'group_5e95d839360db'
			),
			'submit_value'          => __('Confirm', 'om_books'),
			'updated_message'       => __('Review updated', 'om_books'),
			'label_placement'       => 'left',
			'instruction_placement' => 'field',
		));
	}
}

add_action('acf/init', 'om_acf_form_init');

function om_book_add_single_template($om_template)
{
	if (!$om_template) {
		$om_template = OM_BOOKS_PATH . 'templates/single-om_books.php';
	}
	return $template;
}

add_filter('single-om_books_template', 'om_book_add_single_template');

function om_book_add_archive_template($om_template)
{
	if (!$om_template) {
		$om_template = OM_BOOKS_PATH . 'templates/archive-om_books.php';
	}
	return $template;
}

add_filter('archive-om_books_template', 'om_book_add_archive_template');
